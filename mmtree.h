#ifndef MMTREE_H
#define MMTREE_H

#include <vector>
#include "board.h"
#include "common.h"

#include "Thread.h"

/*
 *@brief represents the decision tree for our AI.
 */
class MMTree {
    struct nodeMovePair {
	MMTree* node;
	Move* m;
    };

    int depth; //!< Depth remaining to search. if this is 0, we're a leaf
    Board* board;
    double score;
    Side myColor; //!< The AI color
    bool thisPlayer; //!< true if the AI is making the move
    std::vector<nodeMovePair> children;
    bool pruned; //!< If true, we've pruned this option out.
    bool isRoot; //!< True if we are the root node;
    int turnNum;

    Thread *t;
    
public:
    MMTree(Board* b, int depth, Side ai, bool aiPlays, bool root =false, int tNum =0); //Constructor
    ~MMTree(); //Destructor

    Move* getMove();
    vector<Move *> getMoves();
    MMTree *makeChildren(Move *m);
    void updateChildren(int d);
    void scoreSelf();
    void scoreSelfAB(double alpha, double beta);
    double getScore() {return score;}
    vector<nodeMovePair> getChildren() {return children;}
    Board* getBoard() {return board;}

    void addToChildren(nodeMovePair n) {children.push_back(n);}
    void madeMove(Move* m);

    void prune() {pruned = true;}
    bool isPruned() {return pruned;}
};

struct kidandscore {
    MMTree* child;
    double* score;
};

void* getMoveWrapper(void* args);

#endif
