#include <cstdio>
#include "common.h"
#include "player.h"
#include "board.h"

// Use this file to test your minimax implementation (2-ply depth, with a
// heuristic of the difference in number of pieces).
int main(int argc, char *argv[]) {

    // Create board with example state. You do not necessarily need to use
    // this, but it's provided for convenience.
    Board *board = new Board();

    // Initialize player as the black player, and set testing_minimax flag.
    Player *player = new Player(WHITE);
    player->testingMinimax = true;

    Move *aiMove, *ourMove;
    ourMove = new Move(-1,-1);

    ourMove->setX(3);
    ourMove->setY(2);

    aiMove = player->doMove(ourMove, 0);
    delete aiMove;

    ourMove->setX(1);
    ourMove->setY(2);

    aiMove = player->doMove(ourMove, 0);
    delete aiMove;

    delete ourMove;
    delete player;
    delete board;

    return 0;
}
