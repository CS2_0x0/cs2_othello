#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include "mmtree.h"

using namespace std;

class Player {

private:
	Board *currBoard;
	int searchDepth;
	Side myColor;
	double heatmap[64];

	MMTree* decisionTree;
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    // Helper functions
    void setInternalBoardStateTo(char* bd);
};
#endif
