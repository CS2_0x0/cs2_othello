#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <fstream>
#include <vector>
#include "common.h"
using namespace std;

const double heatmap[64] = {
    0.327999, -0.090445, 0.039883, 0.014393, 0.004407, 0.043631, -0.080611, 0.302597,
    -0.090149, -0.109771, -0.039047, -0.021041, -0.015189, -0.029013, -0.084387, -0.082371,
    0.038417, -0.040535, 0.069465, -0.023899, -0.011249, 0.036829, -0.029949, 0.044129,
    0.015785, -0.023127, -0.021185, 0, 0,          -0.011459, -0.012991, 0.003051,
    0.002571, -0.013783, -0.011559,  0, 0, -0.022757, -0.020093, 0.014223,
    0.044801, -0.030089, 0.037959, -0.009935, -0.024715, 0.069173, -0.039879, 0.039123,
    -0.080403, -0.084887, -0.030237, -0.013739, -0.022359, -0.038809, -0.109505  -0.089845,
    0.300469, -0.079603, 0.044783, 0.003417, 0.014905, 0.037337, -0.089777, 0.328481
};


class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;


    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();

    //Accessors
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
        
    // Game state stuff
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    void setBoard(char data[]);

    // Heat map stuff
    void loadHeatmap();
    const double getHeat(int x, int y);
    double scoreBoard(Side side);

    // Weighting functions
    //double assignWeight(Move *m, int d, Side turn, Side side);
    double getWeight(Side turn, int turnNum=-1);
    double weight_netChange(Side turn);
    double weight_heatMap(Side turn);
    double weight_clumpy(Side turn);

    //printing and debugging
    void printPlain();
};

#endif
