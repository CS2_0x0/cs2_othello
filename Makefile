CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb -O2
OBJS        = player.o board.o mmtree.o
LIBS        = -pthread
PLAYERNAME  = 0x0

all: $(PLAYERNAME) testgame

$(PLAYERNAME): $(OBJS) wrapper.o
	$(CC) $(LIBS) -o $@ $^

testgame: testgame.o
	$(CC) $(LIBS) -o $@ $^

testminimax: $(OBJS) testminimax.o
	$(CC) $(LIBS) -o $@ $^

testmoves: $(OBJS) testmoves.o
	$(CC) $(LIBS) -o $@ $^

%.o: %.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@

java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f *.o $(PLAYERNAME) testgame testminimax testmoves

.PHONY: java testminimax
