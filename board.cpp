#include "board.h"
#include <cstdio>

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    // gamestate
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);

    // heatmap and multipliers **TODO** probs just directly enter
    // numbers in
/*    for (int i = 0; i < 64; i++)
    {
        heatmap[i] = 0;
        black_multiplier[i] = 1;
        white_multiplier[i] = 1;
	}*/
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}

/*
 * @brief Loads a heatmap from a file into an array of doubles
 */
void Board::loadHeatmap() {
/*    std::ifstream file;
    file.open("0x0_gg1e6.csv", std::ios::in);
    for (int i=0; i<64; i++)
    file >> heatmap[i];
    file.close();*/


}

/*
 * @brief returns the value in heatmap at x,y
 */
const double Board::getHeat(int x, int y) {
    return heatmap[y*8+x];
}

/*
 * @brief Returns the score for a given board based on
 * how good it is for player "side"
 *
 * Returns a value between -1 and 1
 */
double Board::scoreBoard(Side side) {
    return this->getWeight(side);
}

/*
 * Takes a potential move and gives it a weighting by recursively taking the
 * minimum weighting among potential next moves. All moves are weighted by
 * comparing to the current gamestate.
 * 
 * Parameters: m is the potential move.
 *             d is the depth remaining to search.
 *             turn is the color making the move.
 *             side is the color we are calculating weightings for
 */
// double Board::assignWeight(Move *m, int d, Side turn, Side side) {
//     Board *newBoard = copy();
//     newBoard->doMove(m, turn);
//     // not finished here: something to do with updating dynamic_multiplier_map?
//     double w;
//     if (d == 0)
//         w = newBoard->getWeight(side);
//     else
//     {
//         Side nextTurn = (turn == BLACK) ? WHITE : BLACK;
//         vector<double> weightings;
//         vector<Move *> possibleMoves;// = newBoard->getMoves(nextTurn);
//         if (possibleMoves.size() == 0)
//         {
//             return newBoard->getWeight(side);
//         }
//         for (int i = 0; i < (int) (possibleMoves.size()); i++)
//         {
//             weightings.push_back(newBoard->assignWeight(possibleMoves[i], d - 1, nextTurn, side));
//         }

//         if (nextTurn == side)
//         {
//             w = -1;
//             for (int i = 0; i < (int) (weightings.size()); i++)
//             {
//                 if (w < weightings[i])
//                     w = weightings[i];
//             }
//         }
//         else
//         {
//             w = 1;
//             for (int i = 0; i < (int) (weightings.size()); i++)
//             {
//                 if (w > weightings[i])
//                     w = weightings[i];
//             }
//         }

//         for (int i = 0; i < (int) (possibleMoves.size()); i++)
//             delete possibleMoves[i];
//     }
//     delete newBoard;
//     return w;
// }

/*
 * @brief Returns the overall rating of a given board.
 *
 * @params[turn] who we should perform the rating for
 * @returns A weighting between -1 and 1
 */
double Board::getWeight(Side turn, int turnNum/* = -1*/) {
    //double cl;
    double nc;
    //double hm;
    //cl = weight_clumpy(turn);
    //clumpy = 0;
    nc = weight_netChange(turn);
    //hm = weight_heatMap(turn);

    double retval; //= 
	//nc * (60 - turnNum)/60.0 +
	//hm * turnNum / 60.0;
    retval = nc;
    return retval;
}

/*
 * @brief Returns an intermediate weighting based on the net
 * change of tiles.
 *
 * @params[turn] who we are performing the rating for
 * @returns A weighting between -1 and 1
 */
double Board::weight_netChange(Side turn) {
    double w = (double) (countBlack() - countWhite());
    w /= (double) (countBlack() + countWhite());
    if (turn == WHITE)
    {
        w *= -1;
    }
    return w;
}
/*
 * @brief Returns an intermediate weighting based on the heat
 * maps
 *
 * @params[turn] who we are performing the rating for
 * @returns A weighting between -1 and 1
 */
double Board::weight_heatMap(Side turn) {
    Side other = (turn == WHITE) ? BLACK : WHITE;
    
    double score = 0;

    for (int i=0; i<8; i++) {
    for (int j=0; j<8; j++) {
        if (get(turn, j, i)) {
        score += getHeat(j,i);
        } else if (get(other, j, i)) {
        score -= getHeat(j,i);
        }
    }
    }

    return score/3.6;//max total score
}

/*
 * @brief Returns an intermediate weighting based on clumpiness
 *
 * @returns A weighting between -1 and 1
 *
 * Basically we sum the total number of edges of adjacent blocks
 * (diagonal counts too), and divide by total number of blocks.
 */
double Board::weight_clumpy(Side turn) {
    Side other = (turn == WHITE) ? BLACK : WHITE;
    Side matching;

    int nCount;
    int totalT, totalO;
    int nT, nO;
    bool matchesMe;
    nT = nO = 0;
    for (int i=0; i<8; i++) {
    for (int j=0; j<8; j++) {
        if (get(turn, j, i)) {
        matching = turn;
        nT++;
        matchesMe = true;
        } else if (get(other, j, i)) {
        matching = other;
        nO++;
        matchesMe = false;
        }
        nCount = 0;
        for (int i1=-1; i1<=1; i1++) {
        for (int j1=-1; j1<=1; j1++) {
            if (j1!=0 || i1!=0) {
            //loop through all neighbors of every square
            if (get(matching, j+j1, i+i1)) nCount++;
            }
        }
        }
        if (matchesMe) totalT += nCount;
        else totalO+=nCount;
    }
    }

    totalT = totalT/2;
    totalO = totalO/2;

    double score = (double)totalT/nT - (double)totalO/nO;
    score = score / 4; //from max degree of fanout
    
    return score;
}

void Board::printPlain() {
    char sq;
    for (int i=0; i<8; i++) {
	for (int j=0; j<8; j++) {
	    if (occupied(j,i)) {
		if (get(WHITE, j, i)) {
		    sq = 'O';
		} else sq = '@';
	    } else sq = '_';
	    printf("%c",sq);
	}
	printf("\n");
    }
}
