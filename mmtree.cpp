#include "mmtree.h"

#include <cmath>

/*
 *@brief Constructs the MMTree
 *
 * Note that the pointer used to construct
 * the tree may be freed at any point.
 */
MMTree::MMTree (Board *b, int depth, Side ai, bool aiPlays, bool root /*= false*/, int tNum/*= 0*/) {
    board = b;
    this->depth = depth;
    score = -INFINITY;
    thisPlayer = aiPlays;
    myColor = ai;
    pruned = false;
    isRoot = root;
    turnNum = tNum;
    updateChildren(depth);
}

/*
 * @brief Destructor for MMTree.
 *
 * Frees all memory INCLUDING the board used to initialize
 * the tree
 */
MMTree::~MMTree () {
    nodeMovePair kid;
    while (children.size() > 0) {
	kid = children[children.size() - 1];
	delete kid.m;
	delete kid.node;
	children.pop_back();
    }
    delete board;
}


/*
 * @brief Returns a pointer to the best move at a certain location
 * 
 * Returns NULL if there are no moves.
 */
Move* MMTree::getMove() {
    t = new Thread[children.size()];
    kidandscore *ks = new kidandscore[children.size()];
    for (int i=0; i<(int)children.size(); i++) {
	ks[i].child = children[i].node;
	ks[i].score = new double;
	(&t[i])->run(getMoveWrapper, (void*)(ks + i));
    }
    for (int i=0; i<(int)children.size(); i++) {
	(&t[i])->join();
    }
    delete[] t;


    Move* tor = NULL;
    double maxScore = -INFINITY;
    double tmpScore;
//    double alpha, beta;
    for (int i=0; i<(int)children.size(); i++) {
//	alpha = -INFINITY;
//	beta = INFINITY;
//	children[i].node->scoreSelfAB(alpha, beta);
//	children[i].node->scoreSelf();
	tmpScore = *(ks[i].score);
	delete ks[i].score;

	if (tmpScore > maxScore) {
	    maxScore = tmpScore;
	    tor = children[i].m;
	}
    }
    delete[] ks;
    return tor;
}

/*
 *@brief Wrapper for the getMove function
 */
void* getMoveWrapper(void* args) {
    double alpha = -INFINITY;
    double beta = INFINITY;
    kidandscore *k = (kidandscore*) args;
    k->child->scoreSelfAB(alpha, beta);
    *(k->score) = k->child->getScore();
    return NULL;
}


/*
 * @brief Returns a list of possible moves given the current board state.
 */
vector<Move *> MMTree::getMoves()
{
    Side side = (thisPlayer) ? myColor : (myColor == BLACK) ? WHITE : BLACK;
    vector<Move *> moves;
    for (int j = 0; j < 8; j++)
    {
        for (int i = 0; i < 8; i++)
        {
            if (!board->occupied(i, j))
            {
                Move *m = new Move(i, j);
                if (board->checkMove(m, side))
                    moves.push_back(m);
                else
                    delete m;
            }
        }
    }
    return moves;
}

/*
 * @brief Assigns a score for itself, using the minimax principals
 * The result is stored in score;
 */
void MMTree::scoreSelf() {
    if (depth == 0) {
	//then we are a leaf
	score = board->getWeight(myColor, turnNum);
	return;
    }
    
    //we want to maximize our score from the children
    //and minimize our score if it's not our turn
    double maxScore = -INFINITY;
    double tmpScore;
    for (int i=0; i<(int)children.size(); i++) {
	children[i].node->scoreSelf();
	//maximizing score*-1 is equivalent to minimizing score
	tmpScore = children[i].node->getScore() * (thisPlayer?1:-1);
	if (tmpScore > maxScore) maxScore = tmpScore;
    }
    score = maxScore * (thisPlayer?1:-1);
}

/*
 * @brief Uses Alpha Beta Pruning to do a modified scoreSelf()
 */
void MMTree::scoreSelfAB(double alpha, double beta) {
    if (depth == 0) {
	score = board->getWeight(myColor, turnNum);
	return;
    }

    double retval;
    bool pruning = false;
    if (thisPlayer) {
	//maximizing player is playing
	for (int i=0; i<(int)children.size(); i++) {
	    if (!pruning) {
		if (!children[i].node->isPruned()) {
		    children[i].node->scoreSelfAB(alpha, beta);
		    retval = children[i].node->getScore();
		    if (retval > alpha) alpha = retval;
		}

//		if (beta <= alpha) pruning = true;
	    } else {
		children[i].node->prune();
	    }
	}
	score = alpha;
    } else {
	//minimizing player is playing
	for (int i=0; i<(int)children.size(); i++) {
	    if (!pruning) {
		if (!children[i].node->isPruned()) {
		    children[i].node->scoreSelfAB(alpha, beta);
		    retval = children[i].node->getScore();
		    if (retval < beta) beta = retval;
		}

		if (beta <= alpha) pruning = true;
	    } else {
		children[i].node->prune();
	    }
	}
	score = beta;
    }	    
    return;
}

/*
 * @brief Calls getMoves and recursively makes children branches until depth = 0
 * @return A pointer to the child.
 */
MMTree * MMTree::makeChildren(Move *m) {
    Side side = (thisPlayer) ? myColor : (myColor == BLACK) ? WHITE : BLACK;
    
    // Make new board representing state after move
    Board *b = board->copy();
    if (m->getX() == -1 && m->getY() == -1) {
	    //do nothing to change the move
    }
    else {
	    b->doMove(m, side);
    }

    // Set the tree's members
    MMTree *child = new MMTree(b, depth - 1, myColor, !thisPlayer, false, turnNum+1);


    // Find the child's children if necessary
    if (child->depth > 0)
    {
        vector<Move *> moves = child->getMoves();
        for (int i = 0; i < (int) moves.size(); i++)
        {
            nodeMovePair tmp;
	        tmp.node = child->makeChildren(moves[i]);
	        tmp.m = moves[i];
            child->addToChildren(tmp);
        }
	    if (moves.size() == 0) {
	        nodeMovePair tmp;
	        tmp.m = new Move(-1,-1);
	        tmp.node = child->makeChildren(tmp.m);
	        child->addToChildren(tmp);
	    }
    }
    return child;
}

/**
 * @brief Updates the children of the current node to
 * reflect the new depth. Ensures we have the tree to
 * the proper depth.
 */
void MMTree::updateChildren(int d) {
    if (pruned && !isRoot) return; //we've stopped caring about this one
    pruned = false;
    
    Side side = (thisPlayer) ? myColor : (myColor == BLACK) ? WHITE : BLACK;

    if ((depth == 0 || children.size() == 0) && d>0) {
        //we haven't given this node children yet. Let's change that
	depth = d;
	vector<Move *> moves = this->getMoves();
	Board *bcpy;
	for (int i=0; i<(int)moves.size(); i++) {
	    nodeMovePair tmp;
	    tmp.m = moves[i];
//	    tmp.node = this->makeChildren(moves[i]);
	    bcpy = board->copy();
	    bcpy->doMove(moves[i], side);
	    tmp.node = new MMTree(bcpy, depth - 1, myColor, !thisPlayer, false, turnNum+1);
	    this->addToChildren(tmp);
	}
	if (moves.size() == 0) {
	    nodeMovePair tmp;
	    tmp.m = new Move(-1,-1);
//	    tmp.node = this->makeChildren(tmp.m);	    
	    bcpy = board->copy();
	    tmp.node = new MMTree(bcpy, depth - 1, myColor, !thisPlayer, false, turnNum+1);
	    this->addToChildren(tmp);
	}
    }
    depth = d;

    for (int i=0; i<(int)children.size(); i++) {
	children[i].node->updateChildren(d-1);
    }
}
    

/*
 * @brief We have made a move, update the tree to reflect that
 */
void MMTree::madeMove(Move* m) {
    //we will assume the tree has already been built, and the move
    //that's been made already matches one of the potential moves
    if (m == NULL){
	m = new Move(-1,-1); //no moves possible
    }

    int matchingIndex = -1;
    Move* checkMove;
    for (int i=0; i<(int)children.size(); i++) {
	checkMove = children[i].m;
	if (checkMove->getX() == m->getX() &&
	    checkMove->getY() == m->getY()) {
	    matchingIndex = i;
	    break;
	}
    }

    if (matchingIndex == -1) {
	//there were no corresponding moves
	return;
    }

    for (int i=children.size()-1; i>=0; i--) {
	if (i != matchingIndex) {
	    delete children[i].m;
	    delete children[i].node;
	    children.erase(children.begin() + i);
	}
    }
	    

    //we now should have only one child
    if (children.size() == 0) return;

    delete board;
    delete children[0].m;
    MMTree *pathToNode = children[0].node;
    thisPlayer = !thisPlayer;
    board = children[0].node->getBoard();
    children = children[0].node->getChildren();
    pathToNode->children.clear();
    pathToNode->board = NULL;
    delete pathToNode;
    depth--;
    //we now look exactly like our one child did
    return;
}
