#include "player.h"
#include <fstream>

#define FIVEMINUTES 300000

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    myColor = side;
    // loadHeatmap();
    currBoard = new Board();
    searchDepth = 8;
    decisionTree = new MMTree(currBoard, searchDepth, side, (side == BLACK), true);
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete decisionTree;
    delete currBoard;
}

/*
 * Sets the internal board state to a particular state
 */
void Player::setInternalBoardStateTo(char* bd) {
    delete decisionTree;
    currBoard = new Board();
    currBoard->setBoard(bd);
    if (testingMinimax) searchDepth = 2;

    decisionTree = new MMTree(currBoard, searchDepth, myColor, true, true);
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    if ((msLeft <= FIVEMINUTES) && (msLeft != -1))
        searchDepth = 3;
    decisionTree->madeMove(opponentsMove);
    Move* bestMove, *bmCpy;
    int depth = 2;
    while (depth < searchDepth) {
	decisionTree->updateChildren(depth);
	bestMove = decisionTree->getMove();
	bmCpy = NULL;
	depth++;
    }
    if (bestMove != NULL) {
	bmCpy = new Move(-1,-1);
	bmCpy->setX(bestMove->getX());
	bmCpy->setY(bestMove->getY());
	if (bmCpy->getX() == -1 && bmCpy->getX() == -1) {
	    delete bmCpy;
	    bmCpy = NULL;
	}
    }
    decisionTree->madeMove(bmCpy);
    return bmCpy;
}
